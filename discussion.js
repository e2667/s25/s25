db.course_bookings.insertMany([
	{"courseId": "C001", "studentId": "S004", "isCompleted": true},
	{"courseId": "C002", "studentId": "S001", "isCompleted": false},
	{"courseId": "C001", "studentId": "S003", "isCompleted": true},
	{"courseId": "C003", "studentId": "S002", "isCompleted": false},
	{"courseId": "C001", "studentId": "S002", "isCompleted": true},
	{"courseId": "C004", "studentId": "S003", "isCompleted": false},
	{"courseId": "C002", "studentId": "S004", "isCompleted": true},
	{"courseId": "C003", "studentId": "S007", "isCompleted": false},
	{"courseId": "C001", "studentId": "S005", "isCompleted": true},
	{"courseId": "C004", "studentId": "S008", "isCompleted": false},
	{"courseId": "C001", "studentId": "S013", "isCompleted": true},
	]);

/*
	Aggregation in MongoDB
		This is the act or process of generating manipulated data and perform operations to create filtered results that helps analyzing data.

	Syntax:
		db.collectionName.aggregate([
			{stage 1},
			{stage 2},
			{stage 3}
		])
*/

/*
	$group
		Syntax:
			{$group: {_id: <id>, fieldResult: "valueResult"}}
			$group: { _id: null, count: { $sum: 1 }} - null will include lahat ng nasa collection / 1 will count the document and give the document's value of 1
*/

db.course_bookings.aggregate([
	{
		$group: { _id: null, count: { $sum: 1 }}
	}
]);

/*
	Syntax:
		{$match: {field: value}}
*/

db.course_bookings.aggregate([
	{
		$match: { "isCompleted": true }
	},
	{
		$group: { _id: "$courseId", total: {$sum: 1} }
	}
]);

/*
	$project
		Syntax:
			{$project: {field: 0 or 1}}
*/

db.course_bookings.aggregate([

	{
		$match: {"isCompleted": true}
	},
	{
		$project: {"studentId": 0}
	}
]);

/*
	$sort 1 for ascending and -1 for descending
		Syntax:
			{$sort: {field: 1/-1}}
*/

db.course_bookings.aggregate([
	{$match: {"isCompleted": true}},
	{$sort: {"courseId": -1}}
]);

db.course_bookings.aggregate([
	{ $match: {"isCompleted": true} },
	{ $sort: {"studentId": 1} },
	{ $project: {"courseId": 1, "studentId": 1, "_id": 0} }
]);

/*
	Start of Mini Activity
*/

db.course_bookings.aggregate([
	{ $match: {"isCompleted": true, "studentId": "S013"}},
	{ $group: {_id: "$studentId", total: {$sum: 1}}}
]);
/*
	Syntax:
		{  $countL <nameOfTheOutputFieldWhichHasItsCountAsItsValue>}
*/
//USing $count
db.course_bookings.aggregate([
	{ $match: {"isCompleted": true, "studentId": "S013"}},
	{ $count: "totalNumberOfCompletedCourse"}
]);


//Sorting
db.course_bookings.aggregate({
	{ $project: {"courseId": -1, "studentId": 1} }
});

//End of Mini Activity


db.orders.insertMany([
	{
		"cust_Id": "A123",
		"amount": 500,
		"status": "A"
	},
	{
		"cust_Id": "A123",
		"amount": 250,
		"status": "A"
	},
	{
		"cust_Id": "B212",
		"amount": 200,
		"status": "A"
	},
	{
		"cust_Id": "B212",
		"amount": 200,
		"status": "D"
	}
]);


//$max
db.orders.aggregate([
		{ $match: {"status": "A"}},
		{ $group: {"_id": "$cust_Id", maxAmount: {$max: "$amount"} }}
	]);

/*
	Group Operators:
		$sum - will total the values
		$max - will show the highest value
		$min - will show the lowest value
		$avg - will show average value
*/


//adding a new operator to save the results to a new collection = $out
/*
	Syntax:
		{ $out: "newCollection"}
*/
db.orders.aggregate([
	{ $match: {"status": "A"}},
	{ $group: {"_id": "$cust_Id", maxAmount: {$max: "$amount"} }}
	{ $out: "aggregatedResults"}
]);